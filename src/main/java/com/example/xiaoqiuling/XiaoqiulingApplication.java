package com.example.xiaoqiuling;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.xiaoqiuling.dao")
public class XiaoqiulingApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaoqiulingApplication.class, args);
    }

}
