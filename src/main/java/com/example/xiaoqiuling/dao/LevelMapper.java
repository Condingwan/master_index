package com.example.xiaoqiuling.dao;

import com.example.xiaoqiuling.db.pojo.Level;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LevelMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Level record);

    int insertSelective(Level record);

    Level selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Level record);

    int updateByPrimaryKey(Level record);
}