package com.example.xiaoqiuling.dao;

import java.util.List;

import com.example.xiaoqiuling.db.pojo.Category;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CategoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);
    
    /**
     * 返回某一根节点下的所有二级子节点
     * @param parentId
     * @return
     */
    List<Category> selectChildrenByParentId(int categoryId);
    
    List<Category> selectAllCategory();
}