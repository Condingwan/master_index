package com.example.xiaoqiuling.db.vo;

import com.example.xiaoqiuling.db.pojo.Order;
import com.example.xiaoqiuling.db.pojo.OrderItem;
import com.example.xiaoqiuling.db.pojo.Shipping;
import lombok.Data;

import java.util.List;

@Data
public class OrderVo {

	private Order order;
	
	private List<OrderItem> orderItemList;
	
	private Shipping shipping;
	
	private String createTime;
}
