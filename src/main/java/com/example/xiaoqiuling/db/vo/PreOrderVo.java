package com.example.xiaoqiuling.db.vo;

import com.example.xiaoqiuling.db.bo.order.CartGoodsBo;
import com.example.xiaoqiuling.db.pojo.Shipping;
import lombok.Data;

import java.util.List;

@Data
public class PreOrderVo {

	private List<CartGoodsBo> cartGoodsBoList;
	
	private List<Shipping> shippingList;

}
