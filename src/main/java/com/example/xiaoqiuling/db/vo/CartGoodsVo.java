package com.example.xiaoqiuling.db.vo;

import com.example.xiaoqiuling.db.bo.CartGoodsBo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CartGoodsVo {

	private List<CartGoodsBo> cartGoodsBoList;
	
	private BigDecimal totalPrice;

}
