package com.example.xiaoqiuling.controller.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.xiaoqiuling.common.Const;
import com.example.xiaoqiuling.common.LoginCheck;
import com.example.xiaoqiuling.common.Message;
import com.example.xiaoqiuling.dao.OrderMapper;
import com.example.xiaoqiuling.db.pojo.Shipping;
import com.example.xiaoqiuling.db.pojo.User;
import com.example.xiaoqiuling.service.interfaces.IOrderService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

	@Autowired
	private IOrderService iOrderService;
	@Autowired
	private LoginCheck loginCheck;
	@Autowired
	private OrderMapper orderMapper;
	
	// 对接生成订单的展示页面：商品信息、收货地址
	@RequestMapping("/go/order/preorder")
	public Message preorder(HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.preorder(user);
	}
	
	@RequestMapping("/go/order/generateOrder")
	public Message generateOrder(Integer shippingId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.generateOrder(shippingId, user);
	}
	
	@RequestMapping("/go/order/saveAddress")
	public Message saveAddress(Shipping shipping, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.saveOrEditAddress(shipping, user);
	}
	
	@RequestMapping("/go/order/editAddress")
	public Message editAddress(Shipping shipping, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.saveOrEditAddress(shipping, user);
	}
	
	@RequestMapping("/*/order/listOrder")
	public Message listOrder(HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.listOrder(user, 1, 10);
	}
	// 1)未付款状态，用户可以取消订单
	// 2)已付款未发货状态，用户可以取消订单（顺带退款）
	@RequestMapping("/go/order/cancelOrder")
	public Message cancelOrder(Integer orderId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.cancelOrder(orderId);
	}
	
	// 已付款未发货的情况，用户申请取消订单(顺带退款)
	@RequestMapping("/go/order/applyRefund")
	public Message applyRefund(Integer orderId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return null;
	}
	
	// 已发货(已付款)的情况，申请退货(退款)
	@RequestMapping("/go/order/applyReturnGoods")
	public Message applyReturnGoods(Integer orderId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return null;
	}
	
	// 前端支付页面获取是否付款的状态，以便跳转至“我的订单”页面
	@RequestMapping("/go/order/getPaymentStatus")
	public Message getPaymentStatus(Integer orderId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iOrderService.getPaymentStatus(orderId, user.getId());
	}
}
