package com.example.xiaoqiuling.service.interfaces;

import com.example.xiaoqiuling.common.Message;
import com.example.xiaoqiuling.db.pojo.Shipping;
import com.example.xiaoqiuling.db.pojo.User;

public interface IOrderService {

	Message preorder(User user);
	
	Message generateOrder(Integer shippingId, User user);
	
	Message saveOrEditAddress(Shipping shipping, User user);
	
	Message listOrder(User user, int pageNum, int pageSize);
	
	Message cancelOrder(Integer orderId);
	
	Message getPaymentStatus(Integer orderId, Integer userId);
}
