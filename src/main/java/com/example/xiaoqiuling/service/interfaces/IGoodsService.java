package com.example.xiaoqiuling.service.interfaces;

import com.github.pagehelper.PageInfo;
import com.example.xiaoqiuling.common.Message;
import com.example.xiaoqiuling.db.pojo.Goods;

public interface IGoodsService {

	Message addGoods(Goods goods);
	
	Message<PageInfo> list(Integer categoryId, int pageNum, int pageSize);
	
	Message getGoodsDetail(int goodsId);
	
	Message<PageInfo> searchGoods(String keyword, int pageNum, int pageSize);
}
