package com.example.xiaoqiuling.service.interfaces;

import com.example.xiaoqiuling.common.Message;
import com.example.xiaoqiuling.db.pojo.User;

public interface IUserService {

	Message register(User user);
	
	Message login(String username, String password);
	
	Message<String> getUsername(String username);
	
	Message getAnswer(String username, String answer);
	
	Message setNewPwd(String username, String password, String uuid);
	
	Message editInfo(User user);
	
	Message getAllInfo(User user);
}
