package com.example.xiaoqiuling.service.interfaces;

import com.example.xiaoqiuling.common.Message;

public interface ICategoryService {

	Message addCategory(int parentId, String name);
	
	Message editCategoryName(int categoryId, String name);
	
	Message getChrildrenCategory(int categoryId);
	
	Message getDeepCategory(int categoryId);
}
