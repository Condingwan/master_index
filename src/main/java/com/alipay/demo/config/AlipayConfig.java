package com.alipay.demo.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author wjm
 * @data 2020/12/30 10:13
 * @desc
 */
public class AlipayConfig {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号,开发时使用沙箱提供的APPID，生产环境改成自己的APPID
    public static String APP_ID = "2021000116679391"; //测试

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCMR6MOxVu7NpSG9Oy/Yyk2Y/ed/qZ+9aLA4XQ7HVCky948h6ALhrFENSQwAFLEWJWNgP/V8qReks8CvyklvlQvIq1oSnN3muvgDjj4ZuzOlU1qu5ZSX6JvZF0hIVznA7/vRxfuCWXc50Wj7k+sTeuvxefp/Atp+yQh7nU/U6C0USligoufjExZAGaox6XLisL/ALS9IA73CRAyqUZ3xPpOuOP/i7xVBEcF4E7kd8J9czzeOqy5Gk/IBW9CM346qSxnnhmfrZqf1ADyeCbpAZ16w+yCEy2iNX70a0mpMksl0y7uTI13ZNbCgRBsmFLyis+6KaFCIGYLwLiHvrgC2lqHAgMBAAECggEAb5+NU3Kd1LHQrDb2sDBCmgIgBjuc1se081AzQdrI0elbdMTjCGjsY375xaxsL8rhbhDpAqCqRIsDaIceQedaa4tneuZLTTAcY1RN5vNXmebaDIUqT2/D/GlbZcJ7f2PPdaIXm5Uk1gu9sX0vLsduNJcBPIIPvwxVKx7pczOjW/dHZ/RLZXKgKBV1C+KsKf3Nh29+CT0ZENEIneDvm0SvDZoLBz/JeHktcU0P5NWXA8qBeBYwjcJt7Yak1lw+EtK0z8mH43fGz50XMgxRjqxUy3x/sgPgb+oPJZyC8oMqcDil1OjHB3xVf7jqjidpxxi+HCg8o1YU50yd6i+Ez0vgyQKBgQDvSN2iJQ7jftfO1ddujfcZG0s+RqFtAvKDqGWpGIOiyPYRU8eKs6k+5hO3PV3cIHwtDobKnbZWLukk0MvJtLnjQ81bR7sX65x+XiAsGC+vOkMhYO8lL1R7a7/DYWPsPXANtsckRQWaD6e2hLYyeUa9iyn6xYlxYV7dAcOj512w+wKBgQCWFEPwDYtKDzPLdxALqjagIROG4oRLz4FyCqva7hni92n6ACIwJYDDRyR2dWFdsIOObnOx3IphNTgafKGxdGkejAHp7P3OCSXqyEiMOb2p+nR50ury5PE6W/wm/JW2D3jrq+us/SlWE/O+3nuSsu81PAK+eCN5mEP0p0+SjLb+5QKBgQDqqYvpauxaAFgGGMKoQgNVFOZKkZvXu2833JquJUNbDjf7MC1dgen1rE8jgcQdj6IX0cowxFA/3st39mgUTO+LyD+JSCRcRgz4BLkKZUHGV53kCxscXXk9fKLYKheDY5ETce+ES0g+GSJBpY7Emxjb47Fso/KxRWtx1DwX7lt8pwKBgHOtNxhGOEBjK1b+dH2pOuV92SE0YyhTzsBp/UCIGafZb/PkOnXF0lOx+CbeHqgqL5yPAdnr1TVL4ex4d+Dhi0yiOa9mndI7NuCac2Cx+MCVbUWhDrsdP9ntoJMkSPjFQDzWjJcPotbYFh3eOn0+MiZkFPFehGlqtaZTd3/LuhkJAoGAenh6dEdjuW7mJbdnLIJy4W5rcpUSaNN7JOuq67i2znOT9tlQy5N7gpn/8Bym0Gk1HMahGL1F54giwgP9ulCssYPKVHhtgCyK4DmKWeD/UaOFZT4ke+QNp0IfIMRJhwH2mGHzRXmZk3r5iWjNWv62pd5zA7CCkk2AbvmsWSmoI8Q=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjEejDsVbuzaUhvTsv2MpNmP3nf6mfvWiwOF0Ox1QpMvePIegC4axRDUkMABSxFiVjYD/1fKkXpLPAr8pJb5ULyKtaEpzd5rr4A44+GbszpVNaruWUl+ib2RdISFc5wO/70cX7gll3OdFo+5PrE3rr8Xn6fwLafskIe51P1OgtFEpYoKLn4xMWQBmqMely4rC/wC0vSAO9wkQMqlGd8T6Trjj/4u8VQRHBeBO5HfCfXM83jqsuRpPyAVvQjN+OqksZ54Zn62an9QA8ngm6QGdesPsghMtojV+9GtJqTJLJdMu7kyNd2TWwoEQbJhS8orPuimhQiBmC8C4h764AtpahwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8088/Alipay/notifyUrl";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://localhost:8088/Alipay/returnUrl";
    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "UTF-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do"; //测试

    // 支付宝网关
    public static String log_path = "E:\\";

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis() + ".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
